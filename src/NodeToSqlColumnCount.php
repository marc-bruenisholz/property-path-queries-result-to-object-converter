<?php

namespace ch\_4thewin\PropertyPathQueriesResultToObjectConverter;

use ch\_4thewin\PropertyPathTreeModels\LeafPropertyPathNode;
use ch\_4thewin\PropertyPathTreeModels\RelationshipPropertyPathNode;
use ch\_4thewin\SqlRelationshipModels\ToOne;
use ch\_4thewin\SqppTreeTraversalModels\NodeInterface;
use ch\_4thewin\TreeTraversal\TreeTraversalInterface;

/**
 * Used to count the columns to skip in SQL result to object conversion
 * if an object does not exist and all corresponding column contents are null.
 */
class NodeToSqlColumnCount implements TreeTraversalInterface
{

    protected int $count = 0;

    public function reset(): void {
        $this->count = 0;
    }

    /**
     * @inheritDoc
     */
    public function preOrder(NodeInterface $node, ?NodeInterface $parentNode, array $branch): bool
    {
        // Follow only ToOne Branches
        if($node instanceof RelationshipPropertyPathNode && $node->getRelationship() instanceof ToOne) {
            // increment by one because of mandatory foreign key column to check for existence
            $this->count = $this->count + 1;
            // TODO ID-Nodes?
            return true;
        } elseif($node instanceof LeafPropertyPathNode && $node->isPartOfRenderedBranch()) {
            // ID nodes won't be traversed here
            $this->count = $this->count + 1;
            return true;
        }
        return false;
    }

    /**
     * @inheritDoc
     */
    public function postOrder(NodeInterface $node, ?NodeInterface $parentNode, array $branch): void
    {
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }


}