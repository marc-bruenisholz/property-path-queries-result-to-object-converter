<?php
/*
 * Copyright 2020 Marc Brünisholz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

namespace ch\_4thewin\PropertyPathQueriesResultToObjectConverter;

use ch\_4thewin\PropertyPathTreeModels\LeafPropertyPathNode;
use ch\_4thewin\PropertyPathTreeModels\ParentPropertyPathNode;
use ch\_4thewin\PropertyPathTreeModels\RelationshipPropertyPathNode;
use ch\_4thewin\SqlRelationshipModels\ManyToMany;
use ch\_4thewin\SqlRelationshipModels\OneToMany;
use ch\_4thewin\SqppSqlExpressionBuildingBlocks\ColumnExpression;
use ch\_4thewin\SqppTreeTraversalModels\NodeInterface;
use ch\_4thewin\TreeTraversal\TreeTraversal;
use ch\_4thewin\TreeTraversal\TreeTraversalInterface;

class PropertyPathRowsToObjectConverter implements TreeTraversalInterface
{
    /** @var array */
    protected array $row;

    protected ParentPropertyPathNode $tree;

    protected int $currentColumnIndex;

    /**
     * For each collection query, there is a foreignKeyToRowsMapping
     * that maps foreign keys to the corresponding rows.
     * @var array
     */
    protected array $foreignKeyToRowsMappings;

    protected array $stack;

    protected array $object;

    protected array $currentObject;

    protected array $idColumnIndexStack;

    protected int $idStackIndex;

    protected NodeToSqlColumnCount $nodeCounter;

    /**
     * For each collection query, there is a foreignKeyToAggregationRowMapping
     * that maps foreign keys to the corresponding aggregation rows.
     * @var array
     */
    protected array $foreignKeyToAggregationRowMappings;

    /**
     * PropertyPathQueriesResultToObjectConverter constructor.
     * @param ParentPropertyPathNode $tree
     * @param array $foreignKeyToRowsMappings Is a list of foreignKeyToRowsMapping. Each element maps foreign keys to corresponding result rows. Use mapRowsToForeignKeys() to create $foreignKeyToRowsMappings.
     * @param array $foreignKeyToAggregationRowMappings
     */
    public function __construct(
        ParentPropertyPathNode $tree,
        // TODO Pass the mappings as trees to avoid having to keep an index
        array                  $foreignKeyToRowsMappings,
        array $foreignKeyToAggregationRowMappings
    )
    {
        $this->nodeCounter = new NodeToSqlColumnCount();
        $this->tree = $tree;
        $this->foreignKeyToRowsMappings = $foreignKeyToRowsMappings;
        $this->foreignKeyToAggregationRowMappings = $foreignKeyToAggregationRowMappings;
    }

    private function handleIdNode(ParentPropertyPathNode $node)
    {
        $idNode = $node->getIdNode();
        if ($idNode !== null) {
            $id = $this->row[$this->currentColumnIndex];
            $this->currentColumnIndex++;
            if ($idNode->isPartOfRenderedBranch()) {
                $this->currentObject[$idNode->getName()] = $id;
            }
        }
    }

    public function convert(array $row, bool $skipFirstColumn = false): array
    {
        $this->row = $row;
        $this->currentColumnIndex = $skipFirstColumn ? 1 : 0;
        $this->stack = [];
        $this->object = [];
        $this->currentObject = &$this->object;
        $this->stack[] = &$this->object;

        $this->idColumnIndexStack = [];
        $this->idStackIndex = 0;

        // Treat id of root specially
        $this->idColumnIndexStack[$this->idStackIndex] = $this->currentColumnIndex;
        $this->idStackIndex++;

        // Handle ID node first if there is one and if it is rendered
        $this->handleIdNode($this->tree);

        (new TreeTraversal($this))->traverse($this->tree);
        return $this->object;
    }


    public function preOrder(NodeInterface $node, ?NodeInterface $parentNode, array $branch): bool
    {
        if ($node instanceof ParentPropertyPathNode) {
            if ($node instanceof RelationshipPropertyPathNode) {
                $relationship = $node->getRelationship();
                if ($relationship instanceof ManyToMany || $relationship instanceof OneToMany) {
                    if ($node->isPartOfRenderedBranch()) {
                        $idColumnIndex = $this->idColumnIndexStack[$this->idStackIndex - 1];

                        $id = $this->row[$idColumnIndex];

                        // It is possible that the id key cannot be found in the mapping.
                        // In such a case, there are no rows belonging to that key
                        // TODO having to keep the index to the collection query should be avoided
                        //  Alternative: Have another tree that is traversed at the same time as this property path tree
                        //  This type of traversal must be by pulling instead of pushing. E.g. $traversal->getNextPreOrderNode()
                        $rows = $this->foreignKeyToRowsMappings[$node->getSqlQueryIndex()][$id] ?? [];

                        $converter = new PropertyPathRowsToObjectConverter(
                            $node,
                            $this->foreignKeyToRowsMappings,
                            $this->foreignKeyToAggregationRowMappings
                        );
                        $objects = [];
                        foreach ($rows as $row) {
                            // First column is skipped as it is not used
                            // The first column contains the foreign key.
                            // With the following line seen above:
                            // $rows = $this->collectionRows[$this->currentCollectionIndex][$id];
                            // all rows belonging to the given foreign key $id are returned.
                            $objects[] = $converter->convert($row, true);
                        }

                        // Add aggregation information for this ToMany relationship
                        // TODO code duplication (similar to root)

                        $aggregateFunctions = $relationship->getAggregateFunctions();
                        if(count($aggregateFunctions)> 0) {
                            $aggregationObject = [];
                            // first column is foreign key. skip it
                            $aggregateColumnIndex = 1;
                            $aggregationRow = $this->foreignKeyToAggregationRowMappings[$node->getSqlQueryIndex()][$id] ?? [];
                            foreach($aggregateFunctions as $aggregateFunction) {
                                $propertyName = $aggregateFunction->getPropertyName();
                                if(!isset($aggregationObject[$propertyName])) {
                                    $aggregationObject[$propertyName] = [];
                                }
                                $functionName  = $aggregateFunction->getFunctionName();
                                if(count($aggregationRow) === 0) {
                                    // TODO verify if it is ok to set result to null if there are no rows to aggregate
                                    $aggregationObject[$propertyName][$functionName] = null;
                                } else {
                                    $aggregationObject[$propertyName][$functionName] = $aggregationRow[$aggregateColumnIndex++];
                                }
                            }
                            $this->currentObject[$node->getName() . '_aggregate'] = $aggregationObject;
                        }


                        $this->currentObject[$node->getName()] = $objects;
                    }
                    return false;
                } else {
                    $this->idColumnIndexStack[$this->idStackIndex] = $this->currentColumnIndex;
                    $this->idStackIndex++;

                    $idNode = $node->getIdNode();

                    $id = $this->row[$this->currentColumnIndex];
                    $this->currentColumnIndex++;
                    if ($id === null) {
                        $this->currentObject[$node->getName()] = null;
                        // Add dummy value to stack because post order is still executed on this "null" node.
                        $this->stack[] = null;

                        // Traverse subtree with different class to count columns to skip
                        // ID node is not contained in getSubNodes, hence it is not counted.
                        // ID column has already been accounted for some lines above
                        $this->nodeCounter->reset();
                        (new TreeTraversal($this->nodeCounter))->traverse($node);
                        $nodeCount = $this->nodeCounter->getCount();
                        $this->currentColumnIndex = $this->currentColumnIndex + $nodeCount;

                        // Returning false prevents traversing the subtree in this traversal.
                        return false;
                    }

                    $this->currentObject[$node->getName()] = [];
                    $this->currentObject = &$this->currentObject[$node->getName()];

                    // If there is actually an object, set the id on it
                    if ($idNode->isPartOfRenderedBranch()) {
                        $this->currentObject[$idNode->getName()] = $id;
                    }

                    $this->stack[] = &$this->currentObject;
                }
            }
        } elseif ($node instanceof LeafPropertyPathNode && $node->isPartOfRenderedBranch()) {
            /** @var ColumnExpression $columnExpression */
            $columnExpression = $node->getColumnExpression();
            // TODO just for this line, package 4thewin/sqpp-sql-expression-building-blocks has
            //  been added as a dependency. Also, $columnExpression is assumed to be ColumnExpression,
            //  although only StringInterface is guaranteed. Review dependency.
            //  Should ormFieldData set on property path nodes so that the converter can use it?
            //  ORM objects from the orm-data-provider-interface package are deliberately converted
            //  into objects of internal types for decoupling though.
            $columnType = $columnExpression->getColumnType();
            $value = $this->row[$this->currentColumnIndex];

            // TODO SQLITE Database returns a number for boolean fields. Check if this works for MariaDB too
            // do not convert null value
            if($columnType === 'boolean' && $value !== null) {
                $value = (bool)$value;
            } elseif($columnType === 'binary' && $value !== null) {
                // Binary values have to be encoded somehow
                $value = bin2hex($value);
            }

            $this->currentColumnIndex++;
            $this->currentObject[$node->getName()] = $value;
        }
        return true;
    }

    public function postOrder(NodeInterface $node, ?NodeInterface $parentNode, array $branch): void
    {
        if ($node instanceof RelationshipPropertyPathNode) {
            $relationship = $node->getRelationship();
            if (!($relationship instanceof ManyToMany || $relationship instanceof OneToMany)) {
                $this->idStackIndex--;
                array_pop($this->stack);
                $this->currentObject = &$this->stack[count($this->stack) - 1];
            }
        }
    }
}