<?php
/*
 * Copyright 2020 Marc Brünisholz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

namespace ch\_4thewin\PropertyPathQueriesResultToObjectConverter;

use ch\_4thewin\PropertyPathTreeModels\IdPropertyPathNode;
use ch\_4thewin\PropertyPathTreeModels\LeafPropertyPathNode;
use ch\_4thewin\PropertyPathTreeModels\RelationshipPropertyPathNode;
use ch\_4thewin\PropertyPathTreeModels\RootPropertyPathNode;
use ch\_4thewin\SqlRelationshipModels\ManyToOne;
use ch\_4thewin\SqlRelationshipModels\OneToMany;
use ch\_4thewin\SqlRelationshipModels\OneToOne;
use ch\_4thewin\SqlSelectModels\Table;
use ch\_4thewin\SqppSqlExpressionBuildingBlocks\ColumnExpression;
use PHPUnit\Framework\TestCase;

use function PHPUnit\Framework\assertEquals;

class PropertyPathQueriesResultToObjectConverterTest extends TestCase
{
    public function testMandatoryIdInToOneRelationship()
    {
        $tree = (new RootPropertyPathNode('person', new Table('person', 'id', 'string')))
            ->setIsPartOfRenderedBranch(true)
            ->addSubNode(
                (new RelationshipPropertyPathNode(
                    'mouth',
                    new OneToOne(
                        new Table('person', 'id', 'string'),
                        'mouthId', 'string',
                        new Table('mouth', 'id', 'string'),
                        false
                    )
                ))
                    ->setIsPartOfRenderedBranch(true)
                    ->addSubNode(
                        (new IdPropertyPathNode('id', new ColumnExpression(
                            new Table('person', 'id', 'string'),
                            'mouthId', 'string'
                        )))->setIsNeededForToOne(true)
                    )
                    ->addSubNode(
                        (new LeafPropertyPathNode('length',
                            new ColumnExpression(
                                new Table('mouth', 'id', 'string'),
                                'length', 'string'
                            )
                        ))->setIsPartOfRenderedBranch(true)
                    )
            );
        $propertyPathQueriesResultToObjectConverter = new PropertyPathRowsToObjectConverter(
            $tree,
            [],
            []
        );
        assertEquals(
            [
                'mouth' => [
                    'length' => 'mouthLength5'
                ]
            ],
            $propertyPathQueriesResultToObjectConverter->convert(['mouthId1', 'mouthLength5'])
        );
    }

    public function testSetNullDirectlyOnPropertyOnMissingNestedRelationshipObject()
    {
        $tree = (new RootPropertyPathNode('person', new Table('person', 'id', 'string')))
            ->setIsPartOfRenderedBranch(true)
            ->addSubNode(
                (new RelationshipPropertyPathNode(
                    'friend',
                    new ManyToOne(
                        new Table('person', 'id', 'string'),
                        'friendId', 'string',
                        new Table('person', 'id', 'string')
                    )
                ))
                    ->setIsPartOfRenderedBranch(true)
                    ->addSubNode(
                        (new IdPropertyPathNode(
                            'id',
                            new ColumnExpression(
                                new Table('person', 'id', 'string'),
                                'id', 'string'
                            )
                        ))->setIsPartOfRenderedBranch(true)

                    )
                    ->addSubNode(
                        (new RelationshipPropertyPathNode(
                            'friend',
                            new ManyToOne(
                                new Table('person', 'id', 'string'),
                                'friendId', 'string',
                                new Table('person', 'id', 'string')
                            )
                        ))
                            ->setIsPartOfRenderedBranch(true)
                            ->addSubNode(
                                (new IdPropertyPathNode(
                                    'id',
                                    new ColumnExpression(
                                        new Table('person', 'id', 'string'),
                                        'id', 'string'
                                    )
                                ))->setIsPartOfRenderedBranch(true)
                            )
                    )
            )
            ->addSubNode(
                (new LeafPropertyPathNode(
                    'name',
                    new ColumnExpression(
                        new Table('person', 'id', 'string'),
                        'name', 'string'
                    )
                ))
                    ->setIsPartOfRenderedBranch(true)
            );
        $propertyPathQueriesResultToObjectConverter = new PropertyPathRowsToObjectConverter(
            $tree,
            [],
            []
        );
        assertEquals(
            [
                'friend' => null,
                'name' => 'Lisa'
            ],
            $propertyPathQueriesResultToObjectConverter->convert([null, null, 'Lisa'])
        );
    }

    public function testSetNullDirectlyOnPropertyOnMissingRelationshipObject()
    {
        $tree = (new RootPropertyPathNode('person', new Table('person', 'id', 'string')))
            ->addSubNode(
                (new RelationshipPropertyPathNode(
                    'friend',
                    new ManyToOne(
                        new Table('person', 'id', 'string'),
                        'friendId', 'string',
                        new Table('person', 'id', 'string')
                    )
                ))
                    ->addSubNode(
                        (new IdPropertyPathNode(
                            'id',
                            new ColumnExpression(
                                new Table('person', 'id', 'string'),
                                'id', 'string'
                            )
                        ))
                    )
            );
        $propertyPathQueriesResultToObjectConverter = new PropertyPathRowsToObjectConverter(
            $tree,
            [],
            []
        );
        assertEquals(
            [
                'friend' => null
            ],
            $propertyPathQueriesResultToObjectConverter->convert([null])
        );
    }

    public function test()
    {
        $tree = (new RootPropertyPathNode('person', new Table('person', 'id', 'string')))
            ->setIsPartOfRenderedBranch(true)
            // It is a requirement for the object converter that there is an ID node when a ToMany relationship exists.
            ->addSubNode(
                (new IdPropertyPathNode(
                    'id',
                    new ColumnExpression(new Table('person', 'id', 'string'), 'id', 'string')
                ))
                    // it is optional to render the id here. When rendered, it is added to the resulting object.
                    ->setIsPartOfRenderedBranch(true)
            )
            ->addSubNode(
                (new LeafPropertyPathNode(
                    'name1',
                    new ColumnExpression(new Table('person', 'id', 'string'), 'name1', 'string')
                ))->setIsPartOfRenderedBranch(true)

            )
            ->addSubNode(
                (new RelationshipPropertyPathNode(
                    'relationshipNode',
                    new ManyToOne(
                        new Table('person', 'id', 'string'),
                        'foreignKeyColumnName', 'string',
                        new Table('inverseTable', 'id', 'string')
                    )
                ))
                    ->setIsPartOfRenderedBranch(true)
                    // ID node for ToMany relationships is mandatory.
                    ->addSubNode(
                        (new IdPropertyPathNode(
                            'id',
                            new ColumnExpression(
                                new Table('inverseTable', 'id', 'string'),
                                'id', 'string',
                            )
                        ))
                            ->setIsPartOfRenderedBranch(true)
                    )
                    ->addSubNode(
                        (new LeafPropertyPathNode(
                            'name',
                            new ColumnExpression(
                                new Table('inverseTable', 'id', 'string'),
                                'name', 'string',
                            )
                        ))->setIsPartOfRenderedBranch(true)

                    )
            )
            ->addSubNode(
                (new RelationshipPropertyPathNode(
                    'anotherToOneRelationship',
                    new ManyToOne(
                        new Table('person', 'id', 'string'),
                        'foreignKeyColumnName2', 'string',
                        new Table('inverseTable2', 'id', 'string')
                    )
                ))
                    // ID node for ToMany relationships is mandatory.
                    ->addSubNode(
                        (new IdPropertyPathNode(
                            'id',
                            new ColumnExpression(
                                new Table('inverseTable2', 'id', 'string'),
                                'id', 'string'
                            )
                        ))->setIsNeededForToMany(true)
                    )
                    ->addSubNode(
                        (new LeafPropertyPathNode(
                            'name',
                            new ColumnExpression(
                                new Table('inverseTable2', 'id', 'string'),
                                'name', 'string'
                            )
                        ))->setIsPartOfRenderedBranch(true)
                    )
            )
            ->addSubNode(
                (new RelationshipPropertyPathNode(
                    'oneToManyRelationshipNode',
                    new OneToMany(
                        new Table('owningTable', 'id', 'string'),
                        'foreignKeyColumnName', 'string',
                        new Table('inverseTable', 'id', 'string')
                    )
                ))
                    ->setIsPartOfRenderedBranch(true)
                    ->setSqlQueryIndex(0)
                    /*->addSubNode(
                        (new IdPropertyPathNode('id', new ColumnExpression(new Table('owningTable', 'id', 'string'), 'id' , 'string')))
                    )*/
                    ->addSubNode(
                        (new LeafPropertyPathNode(
                            'name',
                            new ColumnExpression(
                                new Table('owningTable', 'id', 'string'),
                                'name', 'string'
                            )
                        ))
                            ->setIsPartOfRenderedBranch(true)
                    )
            )
            ->addSubNode(
                (new LeafPropertyPathNode(
                    'name2',
                    new ColumnExpression(
                        new Table('inverseTable', 'id', 'string'),
                        'name2', 'string'
                    )
                ))->setIsPartOfRenderedBranch(true)

            );


        $collectionQueryResult = [[1, 'value4'], [1, 'value5']];

        $foreignKeyToRowsMappings = [
            [
                1 => [
                    [1, 'value4'],
                    [1, 'value5']
                ]
            ]
        ];

        $propertyPathQueriesResultToObjectConverter = new PropertyPathRowsToObjectConverter(
            $tree,
            $foreignKeyToRowsMappings,
            []
        );
//        $object = $propertyPathQueriesResultToObjectConverter->convert([1, 'value1', 2, 'value3', 'value2']);
        $object = $propertyPathQueriesResultToObjectConverter->convert(
            [1, 'value1', 2, 'value3', null, null, 'value2']
        );
        assertEquals(
            [
                'id' => 1,
                'name1' => 'value1',
                'relationshipNode' => [
                    'id' => 2,
                    'name' => 'value3'
                ],
                'anotherToOneRelationship' => null,
                'oneToManyRelationshipNode' => [
                    [
//                        'id' => 3,
                        'name' => 'value4'
                    ],
                    [
//                        'id' => 4,
                        'name' => 'value5'
                    ]
                ],
                'name2' => 'value2'
            ],
            $object
        );
    }

}
